# OpenText AI-Driven Content Advisor Value Calculator

**Example** http://opentext.devitup.site/

Basic Standalone page:

    - dist/index.html

Prepared for usage within existing page, add each to existing page:

    - dist/module.html
    - dist/ar-ot-calc.js
    - dist/ar-ot-calc.css


<br>


---


<br>

This project has been created using **webpack scaffold**, you can run

`` npm run build `` to prepare files for use

`` npm start `` to create dev env
