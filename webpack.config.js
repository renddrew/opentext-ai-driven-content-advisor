const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const prefixer = require('postcss-prefix-selector')

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'ar-ot-calc.js',
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    alias: {
        'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },  

  plugins: [
    new webpack.ProgressPlugin(),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // all options are optional
      filename: 'ar-ot-calc.css',
      chunkFilename: '[id].css',
      ignoreOrder: false, // Enable to remove warnings about conflicting order
    }),

    new HtmlWebpackPlugin({
      template: 'src/content-advisor.html',
      filename: 'index.html', 
      inject: true
    }),

    new HtmlWebpackPlugin({
      template: 'src/module.html',
      filename: 'module.html',  
      inject: false
    }),
  ],

  module: {
    rules: [
      {
        test: /.(js|jsx)$/,
        include: [path.resolve(__dirname, 'src/js')],
        loader: 'babel-loader',
        options: {
          plugins: ['syntax-dynamic-import'],
          presets: [
            [
              '@babel/preset-env',
              {
                modules: false
              }
            ]
          ]
        }
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.scss$/,
          use: [
            MiniCssExtractPlugin.loader,
            { loader: "css-loader", options: {} },
            {
              loader: "postcss-loader",
              options: {
                ident: 'postcss',
                plugins: [
                  prefixer({
                      prefix: '.ar-ot-calc',
                  }),
                  require('autoprefixer')({
                    'browsers': ['> 1%', 'last 2 versions']
                  }),
                ]
              }
            },
            { loader: "sass-loader", options: {} },
          ]
        },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'img/',
              publicPath: 'img/'
        },
      },
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      },
    ]
  },

  optimization: {
    splitChunks: {
      cacheGroups: {
        vendors: {
          priority: -10,
          test: /[\\/]node_modules[\\/]/
        }
      },

      chunks: 'async',
      minChunks: 1,
      minSize: 30000,
      name: true
    }
  },
};
