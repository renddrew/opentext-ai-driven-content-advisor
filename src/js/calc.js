import Vue from 'vue';
import ApexCharts from 'apexcharts';
import VueCurrencyInput from 'vue-currency-input';
Vue.use(VueCurrencyInput, {
  globalOptions:{
    decimalLength: 0
  }
});

new Vue({
  el: '#calculator',
  data: {

    loaded: false,
    hasCalculated: false,
    showGate: false,

    locale: 'en',
    currency: 'USD',

  	tab: 'PD',
    chart: null,

  	showAssumptions: false,

    companyType: 'Public',
    companyRegulated: 'Regulated',
    handlesCustData: 'Yes',

  	// assumptions editable
  	yrHrs: 1875,
  	fteSal_str: '$65,000', // to be string val for currency input
  	manRwTb_str: '$2,500',
  	coorTime: 2,
  	mailRevTime: 3,

  	// assumptions non editable
  	totalBizMailUSA: 84000000000,
  	totalEmploymentUSA: 127000000,
  	elecCoorWk: 2.5, //% of week for elec correspondance

  	// input defaults
    annRevenue_str: '$100,000,000',  // Annual Revenue
    numEmployees: 5000, 	           // Number of Employees
    numCustomers: 20000,             // Number of Customers
    percentKnowledgeWorkers: 20,     // Percentage of Knowledge workers

    // benchmark precentages
    infoSearch: 16,					// time spent searching for info
    searchGather: 20,				// work week spent search and gathering info
    consoAnaly: 10,					// time spent cosolodating and analyizing info
    reducAutoWf: 17.2,			// less time after automation doc workflows
    reducLostDocs: 12.5,		// time reduced for lost/misfiled docs
    reducPostDigAuto: 51.6,	// errors reduced after digitization, automation
    unstrData: 80,					// global data that is unstructured
    taskAutoFutr: 50,				// tasks automated by 2024
    kwAiFutr: 20,					  // KW using AI by 2024
    aiDescWf: 31.4,					// respondants that use AI for decisions in workflows
    wkHrsCollab: 28,				// hours spend each week by KW for email search & collaboration
    dataPerEmp: (15000000000/(22500*5))/5000*0.8, // 21.33,				// estimated total stored data per employee (TB)(80 % unstructured)

    aiImprCons: 20,       			// OT AI driven assumption conservative	
    aiImprLikely: 40				// OT AI driven assumption likely
  },

  computed: {

    fteSal() {
      return this.$parseCurrency(this.fteSal_str, this.locale, this.currency);
    },

    manRwTb() {
      return this.$parseCurrency(this.manRwTb_str, this.locale, this.currency);
    },

    annRevenue() {
      return this.$parseCurrency(this.annRevenue_str, this.locale, this.currency);
    },

  	// quarterly reporting effort per KW
  	qRepEfKW() {
  		return 4*7*8*(this.fteSal / this.yrHrs);
  	}, 

  	mailEmplAnn() {
  		return this.totalBizMailUSA / this.totalEmploymentUSA;
  	},

  	// search and selection effort reduction
  	pdSsC() { 
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.fteSal * (this.searchGather / 100) * (this.aiImprCons / 100);
  	},
  	pdSsL() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.fteSal * (this.searchGather / 100) * (this.aiImprLikely / 100);
  	},

  	// visualization effort reduction
  	pdVisC() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * (this.consoAnaly / 100) * this.fteSal * (this.aiImprCons / 100);
  	},
  	pdVisL() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * (this.consoAnaly / 100) * this.fteSal * (this.aiImprLikely / 100);
  	},

  	// increased discovery accuracy
  	pdDiscoaccC() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.fteSal * (this.reducLostDocs / 100) * (this.infoSearch / 100) * (this.aiImprCons / 100);
  	},
  	pdDiscoaccL() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.fteSal * (this.reducLostDocs / 100) * (this.infoSearch / 100) * (this.aiImprLikely / 100);
  	},

  	// risk reduction
  	pdRrC() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.fteSal * (this.reducPostDigAuto / 100) * (this.aiDescWf / 100) * (this.aiImprCons / 100);
  	},
  	pdRrL() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.fteSal * (this.reducPostDigAuto / 100) * (this.aiDescWf / 100) * (this.aiImprLikely / 100);
  	},


  	// Smart Migration

  	// search, discovery and selection effort reduction
  	smReddsC() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.dataPerEmp * this.manRwTb * (this.aiImprCons / 100) * (this.searchGather / 100);
  	},
  	smReddsL() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.dataPerEmp * this.manRwTb * (this.aiImprLikely / 100) * (this.searchGather / 100);
  	},

  	// elimination of redundant content
  	smElredContC() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.dataPerEmp * this.manRwTb * (this.aiImprCons / 100) * (this.consoAnaly / 100);
  	},
  	smElredContL() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.dataPerEmp * this.manRwTb * (this.aiImprLikely / 100) * (this.consoAnaly / 100);
  	},

  	// better, faster reports for planning
  	smBfrC() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.qRepEfKW * (this.aiImprCons / 100);
  	},
  	smBfrL() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.qRepEfKW * (this.aiImprLikely / 100);
  	},


  	// Processing Correspondence

  	// Ingest processing effort reduction (scanned mail)
  	iperC() {
  		return this.numEmployees * this.mailEmplAnn * this.coorTime / 60 * this.fteSal / this.yrHrs * (this.aiImprCons / 100);
  	},
  	iperL() {
  		return this.numEmployees * this.mailEmplAnn * this.coorTime / 60 * this.fteSal / this.yrHrs * (this.aiImprLikely / 100);
  	},

  	// Review and routing time reduction (scanned mail)
  	rrtrC() {
  		return this.numEmployees * this.mailEmplAnn * this.mailRevTime / 60 * this.fteSal / this.yrHrs * (this.aiImprCons / 100);
  	},
  	rrtrL() {
  		return this.numEmployees * this.mailEmplAnn * this.mailRevTime / 60 * this.fteSal / this.yrHrs * (this.aiImprLikely / 100);
  	},

  	// Elec. correspondance ingest, review and route reduction
  	elcrrrC() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.wkHrsCollab * (this.fteSal / this.yrHrs * 250) * (this.elecCoorWk / 100) * (this.aiImprCons / 100);
  	},
  	elcrrrL() {
  		return this.numEmployees * (this.percentKnowledgeWorkers / 100) * this.wkHrsCollab * (this.fteSal / this.yrHrs * 250) * (this.elecCoorWk / 100) * (this.aiImprLikely / 100);
  	},

  	// Re-work reduction
  	rwrC() {
  		return (this.iperC + this.rrtrC + this.elcrrrC) * (this.reducLostDocs / 100) * (this.aiImprCons / 100);
  	},
  	rwrL() {
  		return (this.iperL + this.rrtrL + this.elcrrrL) * (this.reducLostDocs / 100) * (this.aiImprLikely / 100);
  	},



  	// totals
  	totalCons() {
  		if (this.tab == 'PD') {
  			return this.pdSsC + this.pdVisC + this.pdDiscoaccC + this.pdRrC;
  		}

  		if (this.tab == 'SM') {
  			return this.smReddsC + this.smElredContC + this.smBfrC;
  		}

  		if (this.tab == 'PC') {
  			return this.iperC + this.rrtrC + this.elcrrrC + this.rwrC;
  		}

  		return 0;
  	},
  	totalLikely() {
  		if (this.tab == 'PD') {
  			return this.pdSsL + this.pdVisL + this.pdDiscoaccL + this.pdRrL;
  		} 

  		if (this.tab == 'SM') {
  			return this.smReddsL + this.smElredContL + this.smBfrL;
  		}

  		if (this.tab == 'PC') {
  			return this.iperL + this.rrtrL + this.elcrrrL + this.rwrL;
  		}

  		return 0;
  	},

    ceArr() {
      if (this.tab == 'PD') {
        return [
          this.pdSsC,
          this.pdVisC,
          this.pdDiscoaccC,
          this.pdRrC,
          this.totalCons
        ];
      }

      if (this.tab == 'SM') {
        return [
          this.smReddsC,
          this.smElredContC,
          this.smBfrC,
          this.totalCons
        ];
      }

      if (this.tab == 'PC') {
        return [
          this.iperC,
          this.rrtrC,
          this.elcrrrC,
          this.rwrC,
          this.totalCons,
        ];
      }
    },

    leArr() {
      if (this.tab == 'PD') {
        return [
          this.pdSsL,
          this.pdVisL,
          this.pdDiscoaccL,
          this.pdRrL,
          this.totalLikely,
        ];
      }

      if (this.tab == 'SM') {
        return [
          this.smReddsL,
          this.smElredContL,
          this.smBfrL,
          this.totalLikely,
        ];
      }

      if (this.tab == 'PC') {
        return [
          this.iperL,
          this.rrtrL,
          this.elcrrrL,
          this.rwrL,
          this.totalLikely,
        ]
      }
    },

    graphCategories() {
      if (this.tab == 'PD') {
        return  [
                  'Search and selection effort reduction',
                  'Visualizations effort reduction',
                  'Increased discovery accuracy',
                  'Risk reduction',
                  'TOTAL ESTIMATE'
                ];
      }

      if (this.tab == 'SM') {
        return  [
                  'Search, discovery and selection effort reduction',
                  'Elimination of redundant content',
                  'Better, faster reports for planning',
                  'TOTAL ESTIMATE'
                ];
      }

      if (this.tab == 'PC') {
        return  [
                  'Ingest processing effort reduction (scanned mail)',
                  'Review and routing time reduction (scanned mail)',
                  'Elec. correspondance ingest, review and route reduction',
                  'Re-work reduction',
                  'TOTAL ESTIMATE'
                ];
      }
    },

  },

  methods: {
  	formatNum(num) {
  		num = Math.round(num);
  		return `$${num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  	},

    createChart() {
      var options = {
        colors: ['#09BCEF', '#007b7d'],
          chart: {
              height: 530,
              type: 'bar',
              toolbar: {
              show: false
            },
          },
          plotOptions: {
              bar: {
                  dataLabels: {
                  position: 'top',
              },
            }  
          },
          dataLabels: {
              enabled: false,
              offsetX: 6,
              style: {
                  fontSize: '16px',
                  colors: ['#555']
              }
          },  
          series: [{
              name: 'Conservative Estimate',
              data: this.ceArr,
          },{
              name: 'Likely Estimate',
              data: this.leArr,
          }],
          xaxis: {
              categories: this.graphCategories,
              labels: {
                trim: true,
                maxHeight: 150,
                rotate: -20,
                style: {
                  cssClass: 'mario-chart-xaxis-label',
                  fontSize: '10px',
                  colors: '#2E3D98',
                },
              },
              axisTicks: {
                show: false
              }
          },
          yaxis: {
            tickAmount: 7,
            labels: {
              show: true,
              align: 'right',
              minWidth: 0,
              maxWidth: 160,
              style: {
                  fontSize: '12px',
                  fontFamily: 'Helvetica, Arial, sans-serif',
                  cssClass: 'apexcharts-yaxis-label',
              },
              offsetX: 0,
              offsetY: 0,
              rotate: 0,
              formatter: ((value) => {
                return this.formatNum(value);
              })
            },
          },
         grid: {
          row: {
            colors: ['#fff', '#f2f2f2']
          }
        },
        fill: {
          type: 'gradient',
          gradient: {
            shade: 'light',
            type: "horizontal",
            shadeIntensity: 0.05,
            gradientToColors: undefined,
            inverseColors: true,
            opacityFrom: 0.85,
            opacityTo: 0.85,
            stops: [50, 0, 100]
          },
        },
      }
  
      this.chart = new ApexCharts(
        document.querySelector("#savingsChart"),
        options
      );

      this.chart.render();
    },

    calculate() {
      if (!this.hasCalculated) {
        this.showGate = true;
      }
      this.hasCalculated = true;
    },

    reset() {
      this.hasCalculated = false;
    },

  },

  mounted() {
    window.addEventListener("load", (event) => {
      this.loaded = true;
    });
  },

  updated() {
    if (this.chart) {
      this.chart.updateSeries(
        [{
          name: 'Conservative Estimate',
          data: this.ceArr,
        },{
          name: 'Likely Estimate',
          data: this.leArr,
        }]
      );
    }
  },

  watch: {
    loaded() {
      if (this.loaded) {
        this.createChart();
      } 
    },

    tab() {
      if (this.chart) {
        this.chart.destroy();
        this.createChart();
      }
    },

    percentKnowledgeWorkers(newVal, oldVal) {
      if (newVal > 100) {
        this.percentKnowledgeWorkers = oldVal;
      }
    }
  }

});	










