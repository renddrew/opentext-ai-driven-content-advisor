import he from 'he';

const appFuncs = {

  postRequest(data){
    

  },

  encode(value) {
    if (value) {
      return he.encode(value);
    }
  },

  decode(value) {
    if (value) {
      return he.decode(value, {
        isAttributeValue: false,
      });
    }
    return value;
  },

  shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  },

  startTimer(duration, display, doneCallback) {
    var timer = duration, minutes, seconds;
    var countDown = function () {
      minutes = parseInt(timer / 60, 10)
      seconds = parseInt(timer % 60, 10);

      minutes = minutes < 10 ? "0" + minutes : minutes;
      seconds = seconds < 10 ? "0" + seconds : seconds;

      display.innerText = minutes + ":" + seconds;

      if (--timer < 0) {
        timer = 0;
        if(typeof doneCallback === 'function'){
          clearInterval(countDown);
          doneCallback();
        }
      }
    }
    setInterval(countDown, 1000);
  }


};

export default appFuncs;
